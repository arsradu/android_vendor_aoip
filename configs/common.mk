SUPERUSER_EMBEDDED := true
SUPERUSER_PACKAGE_PREFIX := com.android.settings.cyanogenmod.superuser

# Common overlay
PRODUCT_PACKAGE_OVERLAYS += vendor/aoip/overlay/common

# Common dictionaries
PRODUCT_PACKAGE_OVERLAYS += vendor/aoip/overlay/dictionaries

# Common product property overrides
PRODUCT_PROPERTY_OVERRIDES += \
    keyguard.no_require_sim=true \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
    ro.com.google.clientidbase=android-google \
    ro.com.android.wifi-watchlist=GoogleGuest \
    ro.setupwizard.enterprise_mode=1 \
    ro.com.android.dateformat=MM-dd-yyyy \
    ro.com.android.dataroaming=false

# init.d support
PRODUCT_COPY_FILES += \
    vendor/aoip/prebuilt/common/etc/init.d/00banner:system/etc/init.d/00banner \
    vendor/aoip/prebuilt/common/bin/sysinit:system/bin/sysinit

# userinit support
PRODUCT_COPY_FILES += \
    vendor/aoip/prebuilt/common/etc/init.d/90userinit:system/etc/init.d/90userinit

# AOIP-specific init file
PRODUCT_COPY_FILES += \
    vendor/aoip/prebuilt/common/etc/init.local.rc:root/init.aoip.rc

# Enable SIP+VoIP
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml

# Don't export PS1 in /system/etc/mkshrc.
PRODUCT_COPY_FILES += \
    vendor/aoip/prebuilt/common/etc/mkshrc:system/etc/mkshrc

# T-Mobile theme engine
include vendor/aoip/configs/themes_common.mk

# Optional AOIP packages
PRODUCT_PACKAGES += \
    SoundRecorder \
    Superuser \
    su \
    Basic \
    HALO \
    SamsungServiceMode \
    Trebuchet


# Extra tools
PRODUCT_PACKAGES += \
    openvpn \
    e2fsck \
    mke2fs \
    tune2fs \
    bash \
    nano

# Openssh
PRODUCT_PACKAGES += \
    scp \
    sftp \
    ssh \
    sshd \
    sshd_config \
    ssh-keygen \
    start-ssh

# rsync
PRODUCT_PACKAGES += \
    rsync

# Goo updater app
PRODUCT_COPY_FILES += \
  vendor/aoip/prebuilt/common/app/GooManager_2.1.2.apk:system/app/GooManager_2.1.2.apk

# Inherit common product build prop overrides
-include vendor/aoip/configs/common_versions.mk
