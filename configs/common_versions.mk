# Version information used on all builds
PRODUCT_BUILD_PROP_OVERRIDES += BUILD_DISPLAY_ID=JDQ39 BUILD_ID=JDQ39 BUILD_VERSION_TAGS=release-keys BUILD_UTC_DATE=0

# AOIP branding
AOIP_BUILD_VERSION = OFFICIAL
AOIP_VERSION_MAJOR = 1
AOIP_VERSION_MINOR = 6b2

PRODUCT_PROPERTY_OVERRIDES += \
    ro.aoip.version=AOIP-$(AOIP_VERSION_MAJOR).$(AOIP_VERSION_MINOR)-$(TARGET_PRODUCT)-$(shell date -u +%Y%m%d)

# Goo updater app
PRODUCT_PROPERTY_OVERRIDES += \
    ro.goo.developerid=TeamInsomnia \
	ro.goo.rom=AOIP-$(TARGET_PRODUCT) \
	ro.goo.version=$(shell date -u +%Y%m%d) \
