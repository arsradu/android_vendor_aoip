# Specify phone tech before including full_phone
$(call inherit-product, vendor/aoip/configs/gsm.mk)

# Inherit some common Aoip stuff.
$(call inherit-product, vendor/aoip/configs/common.mk)

# Inherit device configuration
$(call inherit-product, $ device/samsung/ancora/full_ancora.mk)

PRODUCT_RELEASE_NAME := ancora

# Setup device configuration
PRODUCT_NAME := ancora
PRODUCT_DEVICE := ancora
PRODUCT_BRAND := samsung
PRODUCT_MANUFACTURER := samsung
PRODUCT_MODEL := GT-I8150

PRODUCT_BUILD_PROP_OVERRIDES += BUILD_FINGERPRINT=samsung/GT-I8150/GT-I8150:2.3.6/GINGERBREAD/XXLMD:user/release-keys PRIVATE_BUILD_DESC="GT-I8150-user 2.3.6 GINGERBREAD XXLMD release-keys"

# Optional packages
PRODUCT_PACKAGES += \
    LockClock \
    DashClock


# Copy device specific prebuilt files.
PRODUCT_COPY_FILES += \
    vendor/aoip/prebuilt/bootanimations/BOOTANIMATION-480x800.zip:system/media/bootanimation.zip
